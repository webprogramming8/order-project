import { IsNotEmpty } from 'class-validator';
export class CreateOrderItemDto {
  @IsNotEmpty()
  productId: number;
  @IsNotEmpty()
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  customerId: number;
  @IsNotEmpty()
  orderItems: CreateOrderItemDto[];
}
